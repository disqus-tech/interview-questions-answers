package com.disqus.tech.metrosystem.modal;

public final class Card {

	private final String id;
	private double balance;
	private final Passenger passenger;

	public Card(CardBuilder builder) {
		this.id = builder.id;
		this.balance = builder.balance;
		this.passenger = builder.passenger;
	}

	public static CardBuilder newBuilder(String id) {
		return new CardBuilder(id);
	}

	public static class CardBuilder {

		private String id;
		private double balance;
		private Passenger passenger;

		public CardBuilder(String id) {
			this.id = id;
		}

		public CardBuilder balance(double balance) {
			this.balance = balance;
			return this;
		}

		public CardBuilder passenger(Passenger passenger) {
			this.passenger = passenger;
			return this;
		}

		public Card build() {
			return new Card(this);
		}
	}

	public String getId() {
		return id;
	}

	public double getBalance() {
		return balance;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		final Card other = (Card) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}

		return true;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Card [id=" + id + ", balance=" + balance + ", passenger=" + passenger + "]";
	}
}