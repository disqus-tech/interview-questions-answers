package com.disqus.tech.metrosystem.dao.api;

import java.util.List;

import com.disqus.tech.metrosystem.modal.Card;
import com.disqus.tech.metrosystem.modal.CardTransaction;

public interface IDataAccessService {

	boolean saveSwipInTransaction(Card card, CardTransaction tx);

	boolean saveSwipOutTransaction(Card card, CardTransaction tx);

	CardTransaction getSwipInTransaction(Card card);

	List<CardTransaction> getSwipOutTransaction(Card card);
}
