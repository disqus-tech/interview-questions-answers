package com.disqus.tech.metrosystem.service.impl;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import com.disqus.tech.metrosystem.modal.MetroConfData;
import com.disqus.tech.metrosystem.service.api.IFareStrategy;

public class FareStrategyFactory {

	public static IFareStrategy getFareStrategy(final MetroConfData fare, final LocalDateTime localDate) {

		final DayOfWeek today = localDate.getDayOfWeek();

		if (today == DayOfWeek.SATURDAY || today == DayOfWeek.SUNDAY) {
			return new WeekendFareStrategy(fare.getWeekendDayFare());
		} else {
			return new WeekDayFareStrategy(fare.getWeekDayFare());
		}
	}
}
