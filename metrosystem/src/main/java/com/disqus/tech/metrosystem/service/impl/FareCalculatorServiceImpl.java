package com.disqus.tech.metrosystem.service.impl;

import com.disqus.tech.metrosystem.modal.Station;
import com.disqus.tech.metrosystem.service.api.IFareCalculatorService;
import com.disqus.tech.metrosystem.service.api.IFareStrategy;

public class FareCalculatorServiceImpl implements IFareCalculatorService {

	@Override
	public double calculateFare(Station from, Station to, IFareStrategy strategy) {
		final int distance = from.distance(to);
		final double fare = distance * strategy.getFarePerStation();
		return fare;
	}
}
