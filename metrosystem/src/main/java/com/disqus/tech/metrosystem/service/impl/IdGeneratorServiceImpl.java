package com.disqus.tech.metrosystem.service.impl;

import java.util.UUID;
import java.util.concurrent.SynchronousQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.disqus.tech.metrosystem.service.api.IdGeneratorService;

public class IdGeneratorServiceImpl implements IdGeneratorService {

	private static final Logger LOG = LoggerFactory.getLogger(IdGeneratorServiceImpl.class);
	private final SynchronousQueue<String> transactioId = new SynchronousQueue<>();

	public IdGeneratorServiceImpl() {
		startService();
	}

	@Override
	public String getId() {
		try {
			return transactioId.take();
		} catch (InterruptedException e) {
			return getUniqueId();
		}
	}

	private void startService() {
		final Thread idService = new Thread("ID-GENERATOR") {
			public void run() {
				while (true) {
					try {
						final String id = getUniqueId();
						transactioId.put(id);
					} catch (InterruptedException e) {
						LOG.error("Exception in generating id.", e);
					}
				}
			}
		};

		idService.start();
	}

	private String getUniqueId() {
		final UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
