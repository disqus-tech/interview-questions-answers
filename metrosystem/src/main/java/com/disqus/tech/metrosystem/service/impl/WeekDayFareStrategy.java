package com.disqus.tech.metrosystem.service.impl;

import com.disqus.tech.metrosystem.service.api.IFareStrategy;

public class WeekDayFareStrategy implements IFareStrategy {

	private final double farePerStation;

	public WeekDayFareStrategy(final double farePerStation) {
		this.farePerStation = farePerStation;
	}

	@Override
	public String getName() {
		return WeekDayFareStrategy.class.toGenericString();
	}

	@Override
	public double getFarePerStation() {
		return farePerStation;
	}

	@Override
	public String toString() {
		return "WeekDayFareStrategy [farePerStation=" + farePerStation + "]";
	}
}
