package com.disqus.tech.metrosystem.service.api;

public interface IdGeneratorService {

	String getId();
}
