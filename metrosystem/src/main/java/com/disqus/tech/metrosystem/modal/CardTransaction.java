package com.disqus.tech.metrosystem.modal;

import java.time.LocalDateTime;

import com.disqus.tech.metrosystem.service.api.IFareStrategy;

public class CardTransaction {

	private final String id;

	private final Station from;
	private final Station to;
	private final int distance;

	private final LocalDateTime startTime;
	private final LocalDateTime endTime;

	private final double fare;

	private final Card card;
	private final IFareStrategy strategy;

	public CardTransaction(CardTransactionBuilder builder) {

		this.id = builder.id;
		this.from = builder.from;
		this.to = builder.to;
		this.distance = builder.distance;
		this.startTime = builder.startTime;
		this.endTime = builder.endTime;
		this.fare = builder.fare;
		this.card = builder.card;
		this.strategy = builder.usedFareStrategy;
	}

	public static CardTransactionBuilder newBuilder(String txId) {
		return new CardTransactionBuilder(txId);
	}

	public static class CardTransactionBuilder {

		private String id;

		private Station from;
		private Station to;
		private int distance;

		private LocalDateTime startTime;
		private LocalDateTime endTime;

		private double fare;

		private Card card;
		private IFareStrategy usedFareStrategy;

		public CardTransactionBuilder(String id) {
			this.id = id;
		}

		public CardTransactionBuilder from(Station from) {
			this.from = from;
			return this;
		}

		public CardTransactionBuilder to(Station to) {
			this.to = to;
			return this;
		}

		public CardTransactionBuilder distance(int distance) {
			this.distance = distance;
			return this;
		}

		public CardTransactionBuilder startTime(LocalDateTime startTime) {
			this.startTime = startTime;
			return this;
		}

		public CardTransactionBuilder endTime(LocalDateTime endTime) {
			this.endTime = endTime;
			return this;
		}

		public CardTransactionBuilder fare(double fare) {
			this.fare = fare;
			return this;
		}

		public CardTransactionBuilder card(Card card) {
			this.card = card;
			return this;
		}

		public CardTransactionBuilder usedFareStrategy(IFareStrategy usedFareStrategy) {
			this.usedFareStrategy = usedFareStrategy;
			return this;
		}

		public CardTransaction build() {
			return new CardTransaction(this);
		}
	}

	public String getId() {
		return id;
	}

	public Station getFrom() {
		return from;
	}

	public Station getTo() {
		return to;
	}

	public int getDistance() {
		return distance;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public double getFare() {
		return fare;
	}

	public Card getCard() {
		return card;
	}

	public IFareStrategy getUsedFareStrategy() {
		return strategy;
	}

	@Override
	public String toString() {
		return "CardTransaction [id=" + id + ", from=" + from + ", to=" + to + ", distance=" + distance + ", startTime="
				+ startTime + ", endTime=" + endTime + ", fare=" + fare + ", card=" + card + ", strategy=" + strategy
				+ "]";
	}
}
