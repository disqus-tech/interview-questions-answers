package com.disqus.tech.metrosystem;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.disqus.tech.metrosystem.dao.api.IDataAccessService;
import com.disqus.tech.metrosystem.dao.impl.DataAccessServiceImpl;
import com.disqus.tech.metrosystem.exception.MetroSystemException;
import com.disqus.tech.metrosystem.modal.Card;
import com.disqus.tech.metrosystem.modal.CardTransaction;
import com.disqus.tech.metrosystem.modal.MetroConfData;
import com.disqus.tech.metrosystem.modal.Passenger;
import com.disqus.tech.metrosystem.modal.Station;
import com.disqus.tech.metrosystem.service.api.IMetroSystemService;
import com.disqus.tech.metrosystem.service.api.IdGeneratorService;
import com.disqus.tech.metrosystem.service.impl.IdGeneratorServiceImpl;
import com.disqus.tech.metrosystem.service.impl.MetroSystemServiceImpl;
import com.disqus.tech.metrosystem.utils.MetroSystemUtils;

public class MetroSystemApp {
	private static final Logger LOG = LoggerFactory.getLogger(MetroSystemApp.class);

	private static Card[] getTestData(IdGeneratorService idService) {
		final Card[] cards = new Card[5];
		final String[] passengers = { "Alex", "Smith", "Ravi", "Manish", "Bob" };

		for (int i = 0; i < cards.length; i++) {
			final Card card = Card.newBuilder(idService.getId()).balance(100 + (i * 4))
					.passenger(new Passenger((i + 1), passengers[i])).build();
			cards[i] = card;
		}

		return cards;
	}

	public static void main(String[] args) throws MetroSystemException {
		LOG.info("Starting Metro System App, Please wait...");

		if (null == args || args.length == 0) {
			throw new MetroSystemException("Mandatory Args: Config file path missing.");
		}

		final IDataAccessService dataAccessService = new DataAccessServiceImpl();
		final MetroConfData conf = MetroSystemUtils.getMetroConfData(args[0].trim());
		final IdGeneratorService idService = new IdGeneratorServiceImpl();
		final IMetroSystemService metroSystemService = new MetroSystemServiceImpl(dataAccessService, conf, idService);
		LOG.info("Metro System is ready to serve...\n");

		final Card[] cards = getTestData(idService);
		final Station[] from = { Station.A1, Station.A2, Station.A4, Station.A3, Station.A6 };
		final Station[] to = { Station.A10, Station.A9, Station.A6, Station.A7, Station.A3 };

		for (int i = 0; i < cards.length; i++) {
			try {
				metroSystemService.swipeIn(cards[i], from[i], LocalDateTime.now());
				metroSystemService.swipeOut(cards[i], to[i], LocalDateTime.now().plusMinutes(1));
			} catch (Exception e) {
				LOG.error("Exception in processing Passenger " + cards[i].getPassenger(), e);
			}
		}

		for (int i = 0; i < cards.length; i++) {
			final Card card = cards[i];
			final List<CardTransaction> transactions = metroSystemService.getTransactionsSummary(card);
			transactions.stream().forEach(tx -> {
				LOG.info("{} -> {}", card.getPassenger(), tx);
			});
		}
	}
}
