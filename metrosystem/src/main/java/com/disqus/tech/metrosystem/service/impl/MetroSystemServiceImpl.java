package com.disqus.tech.metrosystem.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.disqus.tech.metrosystem.dao.api.IDataAccessService;
import com.disqus.tech.metrosystem.exception.InsufficientCardBalance;
import com.disqus.tech.metrosystem.exception.MetroSystemException;
import com.disqus.tech.metrosystem.exception.MinimumCardBalanceException;
import com.disqus.tech.metrosystem.modal.Card;
import com.disqus.tech.metrosystem.modal.CardTransaction;
import com.disqus.tech.metrosystem.modal.MetroConfData;
import com.disqus.tech.metrosystem.modal.Station;
import com.disqus.tech.metrosystem.service.api.IFareCalculatorService;
import com.disqus.tech.metrosystem.service.api.IFareStrategy;
import com.disqus.tech.metrosystem.service.api.IMetroSystemService;
import com.disqus.tech.metrosystem.service.api.IdGeneratorService;

public class MetroSystemServiceImpl implements IMetroSystemService {

	private static final Logger LOG = LoggerFactory.getLogger(MetroSystemServiceImpl.class);

	private ConcurrentMap<Station, AtomicInteger> stationFootFall = new ConcurrentHashMap<>();

	private final IDataAccessService dataAccessService;
	private final MetroConfData conf;
	private final IdGeneratorService idService;

	public MetroSystemServiceImpl(IDataAccessService dataService, MetroConfData conf, IdGeneratorService idService) {
		this.dataAccessService = dataService;
		this.conf = conf;
		this.idService = idService;
	}

	@Override
	public boolean swipeIn(Card card, Station from, LocalDateTime entry) throws MetroSystemException {

		LOG.info("Request received for swip-in from : {} for card id : {}", from, card.getId());

		final double minBal = conf.getMinBalance();
		if (card.getBalance() < minBal) {
			final String message = "Minimum balance of Rs " + minBal + " is required at Swipe In";
			LOG.error("Card: {} doesn't have minimum balance : {}, Please recharge !", card.getId(), minBal);
			throw new MinimumCardBalanceException(message);
		}

		saveFootFall(from);

		final IFareStrategy strategy = FareStrategyFactory.getFareStrategy(conf, entry);
		final String txId = idService.getId();

		final CardTransaction swipInTx = CardTransaction.newBuilder(txId).from(from).card(card).startTime(entry)
				.usedFareStrategy(strategy).build();
		final boolean status = dataAccessService.saveSwipInTransaction(card, swipInTx);

		LOG.info("Request completed for swip-in from : {} for card id : {} & status : {}", from, card.getId(), status);
		return status;
	}

	@Override
	public boolean swipeOut(Card card, Station to, LocalDateTime exit) throws MetroSystemException {

		LOG.info("Request received for swip-out at : {} for card id : {}", to, card.getId());

		saveFootFall(to);

		final CardTransaction tx = dataAccessService.getSwipInTransaction(card);
		if (null == tx) {
			final String message = "Security Alert ! Card : " + card.getId() + " not swapped in.";
			LOG.error(message);
			throw new MetroSystemException(message);
		}

		final IFareCalculatorService fareCalculatorService = new FareCalculatorServiceImpl();
		final double fare = fareCalculatorService.calculateFare(tx.getFrom(), to, tx.getUsedFareStrategy());

		if (fare > card.getBalance()) {
			LOG.error("Card : {} Insufficient balance for Swipe Out, Please recharge !", card.getId());
			throw new InsufficientCardBalance("Insufficient balance for Swipe Out, Please recharge !");
		}

		try {
			final int distance = to.distance(tx.getFrom());
			final double newBalance = card.getBalance() - fare;
			card.setBalance(newBalance);

			final CardTransaction swipOutTransaction = CardTransaction.newBuilder(tx.getId()).from(tx.getFrom())
					.card(card).startTime(tx.getStartTime()).to(to).endTime(exit).distance(distance).fare(fare)
					.usedFareStrategy(tx.getUsedFareStrategy()).build();

			final boolean status = dataAccessService.saveSwipOutTransaction(card, swipOutTransaction);
			LOG.info("Request completed for swip-out to : {} for card id : {} & status : {}", to, card.getId(), status);
			return status;
		} catch (Exception e) {
			final String message = "Sytem Failure - Swapped out failed for card : " + card.getId();
			LOG.error(message);
			throw new MetroSystemException(message);
		}

	}

	@Override
	public int calculateFootFall(Station station) {
		return stationFootFall.getOrDefault(station, new AtomicInteger(0)).get();
	}

	@Override
	public List<CardTransaction> getTransactionsSummary(Card card) {
		return dataAccessService.getSwipOutTransaction(card);
	}

	private void saveFootFall(Station station) {
		stationFootFall.putIfAbsent(station, new AtomicInteger());
		stationFootFall.get(station).incrementAndGet();
	}
}
