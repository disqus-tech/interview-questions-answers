package com.disqus.tech.metrosystem.exception;

public class InsufficientCardBalance extends MetroSystemException {

	private static final long serialVersionUID = -1097697295484329336L;

	public InsufficientCardBalance(String message) {
		super(message);
	}

	public InsufficientCardBalance(String message, Throwable cause) {
		super(message, cause);
	}
}
