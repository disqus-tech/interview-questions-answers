package com.disqus.tech.metrosystem.exception;

public class MetroSystemException extends Exception {

	private static final long serialVersionUID = -3839344513982856231L;

	public MetroSystemException(String message) {
		super(message);
	}

	public MetroSystemException(String message, Throwable cause) {
		super(message, cause);
	}
}
