package com.disqus.tech.metrosystem.utils;

import static com.disqus.tech.metrosystem.common.CommonConstants.MIN_BALANCE_PROP;
import static com.disqus.tech.metrosystem.common.CommonConstants.WEEKDAY_FARE_PROP;
import static com.disqus.tech.metrosystem.common.CommonConstants.WEEKEND_FARE_PROP;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.disqus.tech.metrosystem.exception.MetroSystemException;
import com.disqus.tech.metrosystem.modal.MetroConfData;

public class MetroSystemUtils {

	private static final Logger LOG = LoggerFactory.getLogger(MetroSystemUtils.class);

	public static MetroConfData getMetroConfData(final String configFile) throws MetroSystemException {
		LOG.info("Request received for reading config data from : {}", configFile);

		Properties properties = new Properties();
		try {
			final InputStream input = new FileInputStream(configFile);
			properties.load(input);
		} catch (Exception e) {
			throw new MetroSystemException("config file doesn't exist at path : " + configFile);
		}

		final double minBalance = getValue(MIN_BALANCE_PROP, properties);
		final double weekDayFare = getValue(WEEKDAY_FARE_PROP, properties);
		final double weekendDayFare = getValue(WEEKEND_FARE_PROP, properties);

		final MetroConfData metroConfData = MetroConfData.newBuilder().minBalance(minBalance).weekDayFare(weekDayFare)
				.weekendDayFare(weekendDayFare).build();

		LOG.info("Configuration for metro system : {}", metroConfData);
		return metroConfData;
	}

	public static double getValue(final String key, final Properties properties) throws MetroSystemException {
		try {
			return Double.parseDouble(properties.getProperty(key).toString());
		} catch (Exception e) {
			throw new MetroSystemException("config file doesn't have property : " + key);
		}
	}

	public static void main(String[] args) {

	}
}
