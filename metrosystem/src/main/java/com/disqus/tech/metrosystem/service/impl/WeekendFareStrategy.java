package com.disqus.tech.metrosystem.service.impl;

import com.disqus.tech.metrosystem.service.api.IFareStrategy;

public class WeekendFareStrategy implements IFareStrategy {

	private final double farePerStation;

	public WeekendFareStrategy(final double farePerStation) {
		this.farePerStation = farePerStation;
	}

	@Override
	public String getName() {
		return WeekendFareStrategy.class.toGenericString();
	}

	@Override
	public double getFarePerStation() {
		return farePerStation;
	}

	@Override
	public String toString() {
		return "WeekendFareStrategy [farePerStation=" + farePerStation + "]";
	}
}
