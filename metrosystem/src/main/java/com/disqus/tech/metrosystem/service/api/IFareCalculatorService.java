package com.disqus.tech.metrosystem.service.api;

import com.disqus.tech.metrosystem.modal.Station;

public interface IFareCalculatorService {

	double calculateFare(Station from, Station to, IFareStrategy strategy);
}
