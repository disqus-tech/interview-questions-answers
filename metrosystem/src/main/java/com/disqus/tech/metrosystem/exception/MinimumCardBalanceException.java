package com.disqus.tech.metrosystem.exception;

public class MinimumCardBalanceException extends MetroSystemException {

	private static final long serialVersionUID = -5045773063930794600L;

	public MinimumCardBalanceException(String message) {
		super(message);
	}

	public MinimumCardBalanceException(String message, Throwable cause) {
		super(message, cause);
	}
}
