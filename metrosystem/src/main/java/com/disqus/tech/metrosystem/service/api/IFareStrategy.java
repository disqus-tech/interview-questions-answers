package com.disqus.tech.metrosystem.service.api;

public interface IFareStrategy {

	String getName();

	double getFarePerStation();
}
