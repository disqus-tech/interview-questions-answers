package com.disqus.tech.metrosystem.common;

public class CommonConstants {

	public static final String WEEKEND_FARE_PROP = "WEEKEND_FARE";
	public static final String WEEKDAY_FARE_PROP = "WEEKDAY_FARE";
	public static final String MIN_BALANCE_PROP = "MIN_BALANCE";

	public static final String REQUEST_ID = "REQUEST_ID";
}
