package com.disqus.tech.metrosystem.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.disqus.tech.metrosystem.dao.api.IDataAccessService;
import com.disqus.tech.metrosystem.modal.Card;
import com.disqus.tech.metrosystem.modal.CardTransaction;

public class DataAccessServiceImpl implements IDataAccessService {

	private static final Logger LOG = LoggerFactory.getLogger(DataAccessServiceImpl.class);

	private final ConcurrentMap<Card, CardTransaction> swipInTx = new ConcurrentHashMap<>();
	private final ConcurrentMap<Card, List<CardTransaction>> swipOutTx = new ConcurrentHashMap<>();

	@Override
	public boolean saveSwipInTransaction(final Card card, final CardTransaction tx) {

		boolean status = true;
		try {
			swipInTx.put(card, tx);
			LOG.info("Swip In Transaction saved sucessfully for card : {}", card.getId());
		} catch (Exception e) {
			LOG.error("Exception in storing swip in transaction", e);
			status = false;
		}
		return status;
	}

	@Override
	public boolean saveSwipOutTransaction(final Card card, final CardTransaction tx) {
		boolean status = true;
		try {
			List<CardTransaction> transactions = swipOutTx.get(card);
			if (null == transactions || transactions.isEmpty()) {
				transactions = new ArrayList<>();
			}
			transactions.add(tx);

			swipOutTx.put(card, transactions);
		} catch (Exception e) {
			LOG.error("Exception in storing swip out transaction", e);
			status = false;
		}
		return status;
	}

	@Override
	public CardTransaction getSwipInTransaction(final Card card) {
		return swipInTx.remove(card);
	}

	@Override
	public List<CardTransaction> getSwipOutTransaction(Card card) {
		return swipOutTx.getOrDefault(card, Collections.emptyList());
	}
}
