package com.disqus.tech.metrosystem.modal;

public final class MetroConfData {

	private final double weekDayFare;
	private final double weekendDayFare;
	private final double minBalance;

	private MetroConfData(final MetroConfDataBuilder builder) {
		this.weekDayFare = builder.weekDayFare;
		this.weekendDayFare = builder.weekendDayFare;
		this.minBalance = builder.minBalance;
	}

	public static MetroConfDataBuilder newBuilder() {
		return new MetroConfDataBuilder();
	}

	public static class MetroConfDataBuilder {

		private double weekDayFare;
		private double weekendDayFare;
		private double minBalance;

		private MetroConfDataBuilder() {

		}

		public MetroConfDataBuilder weekDayFare(final double weekDayFare) {
			this.weekDayFare = weekDayFare;
			return this;
		}

		public MetroConfDataBuilder weekendDayFare(final double weekendDayFare) {
			this.weekendDayFare = weekendDayFare;
			return this;
		}

		public MetroConfDataBuilder minBalance(final double minBalance) {
			this.minBalance = minBalance;
			return this;
		}

		public MetroConfData build() {
			return new MetroConfData(this);
		}
	}

	public double getWeekDayFare() {
		return weekDayFare;
	}

	public double getWeekendDayFare() {
		return weekendDayFare;
	}

	public double getMinBalance() {
		return minBalance;
	}

	@Override
	public String toString() {
		return "MetroConfData [weekDayFare=" + weekDayFare + ", weekendDayFare=" + weekendDayFare + ", minBalance="
				+ minBalance + "]";
	}
}
