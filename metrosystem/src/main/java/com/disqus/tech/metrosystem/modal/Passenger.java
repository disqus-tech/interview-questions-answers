package com.disqus.tech.metrosystem.modal;

public final class Passenger {

	private final long id;
	private final String name;

	public Passenger(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Passenger [id=" + id + ", name=" + name + "]";
	}
}