package com.disqus.tech.metrosystem.service.api;

import java.time.LocalDateTime;
import java.util.List;

import com.disqus.tech.metrosystem.exception.MetroSystemException;
import com.disqus.tech.metrosystem.modal.Card;
import com.disqus.tech.metrosystem.modal.CardTransaction;
import com.disqus.tech.metrosystem.modal.Station;

public interface IMetroSystemService {

	public boolean swipeIn(Card card, Station from, LocalDateTime entry) throws MetroSystemException;

	public boolean swipeOut(Card card, Station to, LocalDateTime exit) throws MetroSystemException;

	int calculateFootFall(Station station);

	List<CardTransaction> getTransactionsSummary(Card card);
}
