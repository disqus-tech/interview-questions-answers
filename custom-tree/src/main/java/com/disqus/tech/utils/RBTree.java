package com.disqus.tech.utils;

import java.util.Comparator;

public class RBTree<T> {

	public static final boolean BLACK = true;
	public static final boolean RED = false;

	private transient int size = 0;
	private transient int modCount = 0;

	private final Comparator<? super T> comparator;
	private transient Node<T> root;

	public RBTree() {
		comparator = null;
	}

	public RBTree(Comparator<? super T> comparator) {
		this.comparator = comparator;
	}

	public T add(T data) {

		Node<T> t = root;
		if (null == t) {
			compare(data, data); // type (and possibly null) check
			root = new Node<T>(data);
			size = 1;
			modCount++;
			return null;
		}

		final Comparator<? super T> cpr = comparator;
		Node<T> parent;
		int cmp;

		if (null != cpr) { // for natural ordering
			do {
				parent = t;
				cmp = cpr.compare(data, t.data);
				if (cmp < 0) {
					t = t.left;
				} else if (cmp > 0) {
					t = t.right;
				} else {
					t.data = data;
					final T oldValue = t.data;
					return oldValue;
				}
			} while (t != null);
		} else {
			if (null == data) {
				throw new NullPointerException();
			}

			@SuppressWarnings("unchecked")
			final Comparable<? super T> value = (Comparable<? super T>) data;
			do {
				parent = t;
				cmp = value.compareTo(t.data);
				if (cmp < 0) {
					t = t.left;
				} else if (cmp > 0) {
					t = t.right;
				} else {
					t.data = data;
					final T oldValue = t.data;
					return oldValue;
				}
			} while (t != null);
		}

		final Node<T> x = new Node<T>(data, parent);
		if (cmp < 0) {
			parent.left = x;
		} else {
			parent.right = x;
		}

		balanceAfterAdd(x);
		size++;
		modCount++;
		return null;
	}

	public boolean remove(T data) {

		if (null == data) {
			return false;
		}

		final Node<T> nodeToDeelete = searchNode(data);
		delete(nodeToDeelete);

		return true;
	}

	public boolean isPresent(T data) {
		if (null != searchNode(data)) {
			return true;
		}

		return false;
	}

	public int getLeafCounts(Node<T> node) {
		if (node == null) {
			return 0;
		}

		if (node.left == null && node.right == null) {
			return 1;
		} else {
			return getLeafCounts(node.left) + getLeafCounts(node.right);
		}
	}

	public String getLeafNodes() {
		final StringBuffer sb = new StringBuffer();
		findLeafNodes(sb, root);
		return sb.toString();
	}

	private void findLeafNodes(StringBuffer sb, Node<T> node) {

		if (node == null) {
			return;
		}

		if (node.left == null && node.right == null) {
			sb.append(node.data);
			sb.append(",");
		}

		findLeafNodes(sb, node.left);
		findLeafNodes(sb, node.right);
	}

	private Node<T> searchNode(T data) {

		if (comparator != null) {
			return searchNodeUsingComparator(data);
		}

		if (data == null) {
			throw new NullPointerException();
		}

		@SuppressWarnings("unchecked")
		Comparable<? super T> k = (Comparable<? super T>) data;

		Node<T> p = root;
		while (p != null) {
			int cmp = k.compareTo(p.data);
			if (cmp < 0) {
				p = p.left;
			} else if (cmp > 0) {
				p = p.right;
			} else {
				return p;
			}
		}

		return null;
	}

	private Node<T> searchNodeUsingComparator(T nodeToSearch) {

		Comparator<? super T> cpr = comparator;
		if (cpr != null) {
			Node<T> p = root;
			while (p != null) {
				int cmp = cpr.compare(nodeToSearch, p.data);
				if (cmp < 0) {
					p = p.left;
				} else if (cmp > 0) {
					p = p.right;
				} else {
					return p;
				}
			}
		}

		return null;
	}

	public void delete(Node<T> p) {

		modCount++;
		size--;

		if (p.left != null && p.right != null) {
			Node<T> s = successor(p);
			p.data = s.data;
			p = s;
		} // p has 2 children

		Node<T> replacement = (p.left != null ? p.left : p.right);

		if (replacement != null) {
			replacement.parent = p.parent;
			if (p.parent == null) {
				root = replacement;
			} else if (p == p.parent.left) {
				p.parent.left = replacement;
			} else {
				p.parent.right = replacement;
			}

			p.left = p.right = p.parent = null;

			if (p.color == BLACK) {
				fixAfterDeletion(replacement);
			}
		} else if (p.parent == null) { // return if we are the only node.
			root = null;
		} else { // No children. Use self as phantom replacement and unlink.
			if (p.color == BLACK) {
				fixAfterDeletion(p);
			}

			if (p.parent != null) {
				if (p == p.parent.left) {
					p.parent.left = null;
				} else if (p == p.parent.right) {
					p.parent.right = null;
				}

				p.parent = null;
			}
		}
	}

	/**
	 * Returns the successor of the specified Entry, or null if no such.
	 */
	private Node<T> successor(Node<T> t) {

		if (t == null) {
			return null;
		} else if (t.right != null) {
			Node<T> p = t.right;
			while (p.left != null) {
				p = p.left;
			}

			return p;
		} else {
			Node<T> p = t.parent;
			Node<T> ch = t;
			while (p != null && ch == p.right) {
				ch = p;
				p = p.parent;
			}

			return p;
		}
	}

	private void fixAfterDeletion(Node<T> x) {

		while (x != root && colorOf(x) == BLACK) {
			if (x == leftOf(parentOf(x))) {
				Node<T> sib = rightOf(parentOf(x));

				if (colorOf(sib) == RED) {
					setColor(sib, BLACK);
					setColor(parentOf(x), RED);
					rotateLeft(parentOf(x));
					sib = rightOf(parentOf(x));
				}

				if (colorOf(leftOf(sib)) == BLACK && colorOf(rightOf(sib)) == BLACK) {
					setColor(sib, RED);
					x = parentOf(x);
				} else {
					if (colorOf(rightOf(sib)) == BLACK) {
						setColor(leftOf(sib), BLACK);
						setColor(sib, RED);
						rotateRight(sib);
						sib = rightOf(parentOf(x));
					}
					setColor(sib, colorOf(parentOf(x)));
					setColor(parentOf(x), BLACK);
					setColor(rightOf(sib), BLACK);
					rotateLeft(parentOf(x));
					x = root;
				}
			} else { // symmetric
				Node<T> sib = leftOf(parentOf(x));

				if (colorOf(sib) == RED) {
					setColor(sib, BLACK);
					setColor(parentOf(x), RED);
					rotateRight(parentOf(x));
					sib = leftOf(parentOf(x));
				}

				if (colorOf(rightOf(sib)) == BLACK && colorOf(leftOf(sib)) == BLACK) {
					setColor(sib, RED);
					x = parentOf(x);
				} else {
					if (colorOf(leftOf(sib)) == BLACK) {
						setColor(rightOf(sib), BLACK);
						setColor(sib, RED);
						rotateLeft(sib);
						sib = leftOf(parentOf(x));
					}
					setColor(sib, colorOf(parentOf(x)));
					setColor(parentOf(x), BLACK);
					setColor(leftOf(sib), BLACK);
					rotateRight(parentOf(x));
					x = root;
				}
			}
		}

		setColor(x, BLACK);
	}

	private void balanceAfterAdd(Node<T> x) {
		x.color = RED;

		while (x != null && x != root && x.parent.color == RED) {
			if (parentOf(x) == leftOf(parentOf(parentOf(x)))) {
				Node<T> y = rightOf(parentOf(parentOf(x)));
				if (colorOf(y) == RED) {
					setColor(parentOf(x), BLACK);
					setColor(y, BLACK);
					setColor(parentOf(parentOf(x)), RED);
					x = parentOf(parentOf(x));
				} else {
					if (x == rightOf(parentOf(x))) {
						x = parentOf(x);
						rotateLeft(x);
					}
					setColor(parentOf(x), BLACK);
					setColor(parentOf(parentOf(x)), RED);
					rotateRight(parentOf(parentOf(x)));
				}
			} else {
				Node<T> y = leftOf(parentOf(parentOf(x)));
				if (colorOf(y) == RED) {
					setColor(parentOf(x), BLACK);
					setColor(y, BLACK);
					setColor(parentOf(parentOf(x)), RED);
					x = parentOf(parentOf(x));
				} else {
					if (x == leftOf(parentOf(x))) {
						x = parentOf(x);
						rotateRight(x);
					}
					setColor(parentOf(x), BLACK);
					setColor(parentOf(parentOf(x)), RED);
					rotateLeft(parentOf(parentOf(x)));
				}
			}
		}

		root.color = BLACK;
	}

	@SuppressWarnings("unchecked")
	final int compare(T data1, T data2) {
		return comparator == null ? ((Comparable<? super T>) data1).compareTo(data2) : comparator.compare(data1, data2);
	}

	private void rotateLeft(Node<T> p) {
		if (p != null) {
			Node<T> r = p.right;
			p.right = r.left;
			if (r.left != null) {
				r.left.parent = p;
			}

			r.parent = p.parent;
			if (p.parent == null) {
				root = r;
			} else if (p.parent.left == p) {
				p.parent.left = r;
			} else {
				p.parent.right = r;
			}

			r.left = p;
			p.parent = r;
		}
	}

	private void rotateRight(Node<T> p) {
		if (p != null) {
			Node<T> l = p.left;
			p.left = l.right;
			if (l.right != null) {
				l.right.parent = p;
			}
			l.parent = p.parent;
			if (p.parent == null) {
				root = l;
			} else if (p.parent.right == p) {
				p.parent.right = l;
			} else {
				p.parent.left = l;
			}

			l.right = p;
			p.parent = l;
		}
	}

	public static <T> Node<T> leftLeafNode(Node<T> p) {

		Node<T> node = p;
		if (null != node.left) {
			while (node.left != null) {
				node = node.left;
			}
			return node;
		}

		return null;
	}

	private static <T> Node<T> parentOf(Node<T> p) {
		return (p == null ? null : p.parent);
	}

	private static <T> Node<T> leftOf(Node<T> p) {
		return (p == null) ? null : p.left;
	}

	private static <T> Node<T> rightOf(Node<T> p) {
		return (p == null) ? null : p.right;
	}

	private static <T> boolean colorOf(Node<T> p) {
		return (p == null ? BLACK : p.color);
	}

	private static <T> void setColor(Node<T> p, boolean newColor) {
		if (p != null) {
			p.color = newColor;
		}
	}

	static final class Node<T> {
		T data;
		Node<T> left;
		Node<T> right;
		Node<T> parent;
		boolean color = BLACK;

		Node(T data) {
			this.data = data;
		}

		Node(T data, Node<T> parent) {
			this.data = data;
			this.parent = parent;
		}

		@Override
		public String toString() {
			return String.valueOf(data);
		}
	}
}
