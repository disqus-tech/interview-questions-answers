package com.disqus.tech.utils;

public class TreeAppMain {
	public static void main(String[] args) {
		final RBTree<Integer> tree = new RBTree<>();
		final Integer[] nums = { 2, 4, 1, 6, 3, 7, 9, 5 };
		for (int i = 0; i < nums.length; i++) {
			tree.add(nums[i]);
		}
	}
}
